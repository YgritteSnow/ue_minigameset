
#include "MinigameResource_Hanoi.h"
#include "MinigameSetBlock.h"
#include "MinigameSetBlockGrid.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/StaticMesh.h"
#include "Materials/MaterialInstance.h"
#include "Materials/MaterialInstanceDynamic.h"

extern FLogCategoryLogJJ LogJJ;

AMinigame_Hanoi_Brick_3D::AMinigame_Hanoi_Brick_3D(const FObjectInitializer& initializer)
	: Super(initializer)
	, bIsMoving(false)
	, TargetMovingPos(FVector::ZeroVector)
	, TimeRemain(0)
	, bIsPressing(false)
	, bIsInside(false)
{
	PrimaryActorTick.bCanEverTick = true;

	// Structure to hold one-time initialization
	struct FConstructorStatics
	{
		ConstructorHelpers::FObjectFinderOptional<UStaticMesh> PlaneMesh;
		ConstructorHelpers::FObjectFinderOptional<UMaterial> BaseMaterial;
		ConstructorHelpers::FObjectFinderOptional<UMaterialInstance> BlueMaterial;
		ConstructorHelpers::FObjectFinderOptional<UMaterialInstance> OrangeMaterial;
		FConstructorStatics()
			: PlaneMesh(TEXT("/Game/Puzzle/Meshes/PuzzleCube.PuzzleCube"))
			, BaseMaterial(TEXT("/Game/Puzzle/Meshes/BaseMaterial.BaseMaterial"))
			, BlueMaterial(TEXT("/Game/Puzzle/Meshes/BlueMaterial.BlueMaterial"))
			, OrangeMaterial(TEXT("/Game/Puzzle/Meshes/OrangeMaterial.OrangeMaterial"))
		{
		}
	};
	static FConstructorStatics ConstructorStatics;

	// Create dummy root scene component
	DummyRoot = CreateDefaultSubobject<USceneComponent>(TEXT("Dummy0"));
	RootComponent = DummyRoot;

	// Create static mesh component
	BlockMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BlockMesh0"));
	BlockMesh->SetStaticMesh(ConstructorStatics.PlaneMesh.Get());
	BlockMesh->SetRelativeScale3D(FVector(0.2f, 0.2f + GetIndex() * LengthParam, 0.25f));
	BlockMesh->SetRelativeLocation(FVector(0.f, 0.f, 25.f));
	BlockMesh->SetMaterial(0, ConstructorStatics.BlueMaterial.Get());
	BlockMesh->SetupAttachment(DummyRoot);
	BlockMesh->OnClicked.AddDynamic(this, &AMinigame_Hanoi_Brick_3D::HandlePress);
	BlockMesh->OnReleased.AddDynamic(this, &AMinigame_Hanoi_Brick_3D::HandleRelease);
	BlockMesh->OnBeginCursorOver.AddDynamic(this, &AMinigame_Hanoi_Brick_3D::HandleEnter);
	BlockMesh->OnEndCursorOver.AddDynamic(this, &AMinigame_Hanoi_Brick_3D::HandleLeave);

	// Save a pointer to the orange material
	BaseMaterial = ConstructorStatics.BaseMaterial.Get();
	BlueMaterial = ConstructorStatics.BlueMaterial.Get();
	OrangeMaterial = ConstructorStatics.OrangeMaterial.Get();
}

void AMinigame_Hanoi_Brick_3D::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (bIsMoving)
	{
		if (TimeRemain <= 0)
		{
			bIsMoving = false;
			MoveToPosInner(TargetMovingPos);

			//BlockMesh->SetMaterial(0, BaseMaterial);
		}
		else
		{
			MoveToPosInner(FMath::Lerp(GetActorLocation(), TargetMovingPos, DeltaSeconds/TimeRemain));
		}
		TimeRemain -= DeltaSeconds;
	}

	if (bIsPressing)
	{
		bIsMoving = false;
	}
}

void AMinigame_Hanoi_Brick_3D::MoveToPos(int dsc_col, int dsc_idx, float time)
{
	const float XOffset = dsc_idx * BrickWidth - PosOffset.Y + BrickInsideOffset;
	const float YOffset = dsc_col * ColumeWidth - PosOffset.X;

	TargetMovingPos = FVector(XOffset, YOffset, 10.f);

	if (FMath::IsNearlyEqual(time, 0))
	{
		bIsMoving = false;
		MoveToPosInner(TargetMovingPos);
	}
	else
	{
		bIsMoving = true;
		TimeRemain = time;
	}
}
void AMinigame_Hanoi_Brick_3D::MoveToPosInner(const FVector& pos)
{
	SetActorLocation(pos);
}

void AMinigame_Hanoi_Brick_3D::OnChangeIndex()
{
	BlockMesh->SetRelativeScale3D(FVector(0.2f, 0.2f + GetIndex() * LengthParam, 0.25f));
}

void AMinigame_Hanoi_Brick_3D::HandlePress(UPrimitiveComponent* ClickedComp, FKey ButtonClicked)
{
	//UE_LOG(LogJJ, Warning, TEXT("Brick HandlePress: %d - %s"), GetIndex(), *ButtonClicked.ToString());
	SetIsPressing(true);

	OnMouseDrag.ExecuteIfBound();
}
void AMinigame_Hanoi_Brick_3D::HandleRelease(UPrimitiveComponent* ClickedComp, FKey ButtonClicked)
{
	//UE_LOG(LogJJ, Warning, TEXT("Brick HandleRelease: %d - %s"), GetIndex(), *ButtonClicked.ToString());
	SetIsPressing(false);

	OnMouseDrop.ExecuteIfBound();
}
void AMinigame_Hanoi_Brick_3D::HandleLeave(UPrimitiveComponent* ClickedComp)
{
	//UE_LOG(LogJJ, Warning, TEXT("Brick HandleLeave: %d"), GetIndex());
	if (bIsPressing)
	{
		SetIsPressing(false);

		OnMouseDrop.ExecuteIfBound();
	}

	SetIsInside(false);
}
void AMinigame_Hanoi_Brick_3D::HandleEnter(UPrimitiveComponent* ClickedComp)
{
	//UE_LOG(LogJJ, Warning, TEXT("Brick HandleEnter: %d"), GetIndex());
	if (bIsPressing)
		return;

	SetIsInside(true);
}

void AMinigame_Hanoi_Brick_3D::OnFingerPressedBlock(ETouchIndex::Type FingerIndex, UPrimitiveComponent* TouchedComponent)
{
	UE_LOG(LogJJ, Warning, TEXT("OnFingerPressedBlock: %d"), (int)FingerIndex);
}

void AMinigame_Hanoi_Brick_3D::SetIsInside(bool bIn)
{
	if (bIsInside ^ bIn)
	{
		bIsInside = bIn;
		RefreshHighlight();
	}
}

void AMinigame_Hanoi_Brick_3D::SetIsPressing(bool b)
{
	if (bIsPressing ^ b)
	{
		bIsPressing = b;
		RefreshHighlight();
	}
}

void AMinigame_Hanoi_Brick_3D::RefreshHighlight()
{
	if (!bIsInside && !bIsPressing)
	{
		BlockMesh->SetMaterial(0, BlueMaterial);
	}
	else if (bIsInside && !bIsPressing)
	{
		BlockMesh->SetMaterial(0, BaseMaterial);
	}
	else if(bIsPressing)
	{
		BlockMesh->SetMaterial(0, OrangeMaterial);
	}
}


/*
 * AMinigame_Hanoi_Colume_3D
 */
AMinigame_Hanoi_Colume_3D::AMinigame_Hanoi_Colume_3D(const FObjectInitializer& initializer)
	: Super(initializer)
	, bIsSelecting(false)
	, bIsInside(false)
{
	PrimaryActorTick.bCanEverTick = true;

	// Structure to hold one-time initialization
	struct FConstructorStatics
	{
		ConstructorHelpers::FObjectFinderOptional<UStaticMesh> PlaneMesh;
		ConstructorHelpers::FObjectFinderOptional<UMaterial> BaseMaterial;
		ConstructorHelpers::FObjectFinderOptional<UMaterialInstance> BlueMaterial;
		ConstructorHelpers::FObjectFinderOptional<UMaterialInstance> OrangeMaterial;
		FConstructorStatics()
			: PlaneMesh(TEXT("/Game/Puzzle/Meshes/PuzzleCube.PuzzleCube"))
			, BaseMaterial(TEXT("/Game/Puzzle/Meshes/BaseMaterial.BaseMaterial"))
			, BlueMaterial(TEXT("/Game/Puzzle/Meshes/BlueMaterial.BlueMaterial"))
			, OrangeMaterial(TEXT("/Game/Puzzle/Meshes/OrangeMaterial.OrangeMaterial"))
		{
		}
	};
	static FConstructorStatics ConstructorStatics;

	// Create dummy root scene component
	DummyRoot = CreateDefaultSubobject<USceneComponent>(TEXT("Dummy0"));
	RootComponent = DummyRoot;

	// Create static mesh component
	BlockMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BlockMesh0"));
	BlockMesh->SetStaticMesh(ConstructorStatics.PlaneMesh.Get());
	BlockMesh->SetRelativeScale3D(FVector(5.f, 1.f, 0.1f));
	BlockMesh->SetRelativeLocation(FVector(0.f, 0.f, 25.f));
	BlockMesh->SetMaterial(0, ConstructorStatics.BlueMaterial.Get());
	BlockMesh->SetupAttachment(DummyRoot);
	BlockMesh->OnClicked.AddDynamic(this, &AMinigame_Hanoi_Colume_3D::HandlePress);

	// Save a pointer to the orange material
	BaseMaterial = ConstructorStatics.BaseMaterial.Get();
	BlueMaterial = ConstructorStatics.BlueMaterial.Get();
	OrangeMaterial = ConstructorStatics.OrangeMaterial.Get();
}

void AMinigame_Hanoi_Colume_3D::SetBrickInside(bool bValue)
{
	if (bIsInside ^ bValue)
	{
		bIsInside = bValue;
		RefreshHighlight();
	}
}

void AMinigame_Hanoi_Colume_3D::SetSelect(bool bValue)
{
	if (bIsSelecting ^ bValue)
	{
		bIsSelecting = bValue;
		RefreshHighlight();
	}
}

bool AMinigame_Hanoi_Colume_3D::IsPosInside(FVector2D pos)
{
	return pos.X > GetIndex() * ColumeWidth && pos.X < (GetIndex() + 1) * ColumeWidth;
}

void AMinigame_Hanoi_Colume_3D::HandlePress(UPrimitiveComponent* ClickedComp, FKey ButtonClicked)
{
	UE_LOG(LogJJ, Warning, TEXT("Colume HandlePress: %d - %s"), GetIndex(), *ButtonClicked.ToString());

	OnMouseClick.ExecuteIfBound();
}

void AMinigame_Hanoi_Colume_3D::OnFingerPressedBlock(ETouchIndex::Type FingerIndex, UPrimitiveComponent* TouchedComponent)
{
	UE_LOG(LogJJ, Warning, TEXT("OnFingerPressedBlock: %d"), (int)FingerIndex);
}

void AMinigame_Hanoi_Colume_3D::RefreshHighlight()
{
	if (!bIsInside && !bIsSelecting)
	{
		BlockMesh->SetMaterial(0, BlueMaterial);
	}
	else if (bIsInside && !bIsSelecting)
	{
		BlockMesh->SetMaterial(0, BaseMaterial);
	}
	else if (bIsSelecting)
	{
		BlockMesh->SetMaterial(0, OrangeMaterial);
	}
}
