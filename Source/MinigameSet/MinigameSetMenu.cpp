// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "MinigameSetMenu.h"
#include "Blueprint/WidgetTree.h"
#include "Components/VerticalBox.h"
#include "Components/PanelWidget.h"
#include "Components/TextBlock.h"
#include "MinigameLogic/Public/Minigame_CombineToTen.h"

#define LOCTEXT_NAMESPACE "Minigame"

UCustomClickButton::UCustomClickButton(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer)
{}

#if WITH_EDITOR
const FText UCustomClickButton::GetPaletteCategory()
{
	return LOCTEXT("Common", "Mine");
}
#endif

AMinigameSetMenu::AMinigameSetMenu(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

void AMinigameSetMenu::OnBegin()
{
	InitMenuWidget();
	InitScoreWidget();
	InitResultWidget();
}

void AMinigameSetMenu::InitMenuWidget()
{
	MenuWidget = CreateWidget(GetWorld(), MenuWidgetClass, FName("GameMenuWidget"));
	if (!MenuWidget)
		return;

	MenuWidget->AddToViewport();
	MenuWidget->SetVisibility(ESlateVisibility::SelfHitTestInvisible);

	UVerticalBox* list_widget = Cast<UVerticalBox>(MenuWidget->WidgetTree->FindWidget(FName("List_Game")));
	if (!list_widget)
		return;

	UUserWidget* btn_userWidget = Cast<UUserWidget>(list_widget->FindChildByName(FString(TEXT("MenuItemTempl"))));
	if (!btn_userWidget)
		return;

	for (auto It = GameEntryClasses.CreateConstIterator(); It; ++It)
	{
		UUserWidget* game_widget = CreateWidget(MenuWidget, btn_userWidget->GetClass(), It->GetDefaultObject()->GetGameName());
		game_widget->SetVisibility(ESlateVisibility::SelfHitTestInvisible);

		UTextBlock* txt_widget = Cast<UTextBlock>(game_widget->WidgetTree->FindWidget(TEXT("Txt_Name")));
		if (txt_widget)
		{
			txt_widget->SetText(FText::FromName(It->GetDefaultObject()->GetGameName()));
		}

		UCustomClickButton* game_button = Cast<UCustomClickButton>(game_widget->WidgetTree->FindWidget(TEXT("Btn_Game")));
		if (game_button)
		{
			int idx = It.GetIndex();
			game_button->SetOnClicked(FOnClicked::CreateLambda([this, idx]() {
				DoSelectGame(idx);
				return FReply::Handled();
			}));
		}

		list_widget->AddChildToVerticalBox(game_widget);
	}
}

void AMinigameSetMenu::InitResultWidget()
{
	ResultWidget = CreateWidget(GetWorld(), ResultWidgetClass, FName("GameResultWidget"));
	ResultWidget->AddToViewport();
	ResultWidget->SetVisibility(ESlateVisibility::Collapsed);

	UCustomClickButton* return_btn = Cast<UCustomClickButton>(ResultWidget->WidgetTree->FindWidget(FName("Btn_Return")));
	if (return_btn)
	{
		return_btn->SetOnClicked(FOnClicked::CreateLambda([this]() {
			EndCurrentGame();
			OnShowMenu(true);
			return FReply::Handled();
		}));
	}

	UCustomClickButton* restart_btn = Cast<UCustomClickButton>(ResultWidget->WidgetTree->FindWidget(FName("Btn_Restart")));
	if (restart_btn)
	{
		restart_btn->SetOnClicked(FOnClicked::CreateLambda([this]() {
			RestartCurrentGame();
			OnShowMenu(false);
			return FReply::Handled();
		}));
	}
}

void AMinigameSetMenu::InitScoreWidget()
{
	ScoreWidget = CreateWidget(GetWorld(), ScoreWidgetClass, FName("GameScoreWidget"));
	if (!ScoreWidget)
		return;

	ScoreWidget->AddToViewport();
	ScoreWidget->SetVisibility(ESlateVisibility::Collapsed);

	UCustomClickButton* close_btn = Cast<UCustomClickButton>(ScoreWidget->WidgetTree->FindWidget(FName("Btn_Close")));
	if (close_btn)
	{
		close_btn->SetOnClicked(FOnClicked::CreateLambda([this]() {
			EndCurrentGame();
			return FReply::Handled();
		}));
	}

	UCustomClickButton* restart_btn = Cast<UCustomClickButton>(ScoreWidget->WidgetTree->FindWidget(FName("Btn_Restart")));
	if (restart_btn)
	{
		restart_btn->SetOnClicked(FOnClicked::CreateLambda([this]() {
			RestartCurrentGame();
			return FReply::Handled();
		}));
	}
}

void AMinigameSetMenu::OnShowMenu(bool bShow)
{
	MenuWidget->SetVisibility(bShow ? ESlateVisibility::SelfHitTestInvisible : ESlateVisibility::Collapsed);
	ScoreWidget->SetVisibility(!bShow ? ESlateVisibility::SelfHitTestInvisible : ESlateVisibility::Collapsed);
	ResultWidget->SetVisibility(ESlateVisibility::Collapsed);
}

void AMinigameSetMenu::OnChangeScore(FName name, FText desc, FString score_str)
{
	UUserWidget* scoreItem_widget = Cast<UUserWidget>(ScoreWidget->WidgetTree->FindWidget(name));
	if (!scoreItem_widget)
	{
		UVerticalBox* list_widget = Cast<UVerticalBox>(ScoreWidget->WidgetTree->FindWidget(FName("List_Score")));
		if (!list_widget)
			return;

		UUserWidget* score_templ = Cast<UUserWidget>(list_widget->FindChildByName(FString(TEXT("ScoreItem"))));
		if (!score_templ)
			return;

		scoreItem_widget = CreateWidget(ScoreWidget, score_templ->GetClass(), name);
		scoreItem_widget->SetVisibility(ESlateVisibility::SelfHitTestInvisible);
		list_widget->AddChildToVerticalBox(scoreItem_widget);
	}
	UTextBlock* txt_desc_widget = Cast<UTextBlock>(scoreItem_widget->WidgetTree->FindWidget(TEXT("Txt_Desc")));
	if (txt_desc_widget)
	{
		txt_desc_widget->SetText(desc);
	}

	UTextBlock* txt_score_widget = Cast<UTextBlock>(scoreItem_widget->WidgetTree->FindWidget(TEXT("Txt_Score")));
	if (txt_score_widget)
	{
		txt_score_widget->SetText(FText::FromString(score_str));
	}
}

void AMinigameSetMenu::OnResult(bool bWin)
{
	ResultWidget->SetVisibility(ESlateVisibility::SelfHitTestInvisible);
	MenuWidget->SetVisibility(ESlateVisibility::Collapsed);
	ScoreWidget->SetVisibility(ESlateVisibility::Collapsed);

	UTextBlock* txt_result_widget = Cast<UTextBlock>(ResultWidget->WidgetTree->FindWidget(TEXT("Txt_Result")));
	if (txt_result_widget)
	{
		txt_result_widget->SetText(bWin ? LOCTEXT("MinigameMenu", "You Win !") : LOCTEXT("MinigameMenu", "You Lose !"));
	}
}

#undef LOCTEXT_NAMESPACE