// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "UMG/Public/Blueprint/UserWidget.h"
#include "MinigameLogic/Public/MinigameMenuInterface.h"
#include "Components/Button.h"
#include "Framework/SlateDelegates.h"
#include "Widgets/Input/SButton.h"
#include "MinigameSetMenu.generated.h"

UCLASS(BlueprintType)
class UCustomClickButton : public UButton
{
	GENERATED_UCLASS_BODY()

#if WITH_EDITOR
public:
	virtual const FText GetPaletteCategory() override;
#endif

protected:
	virtual TSharedRef<SWidget> RebuildWidget() override
	{
		auto res = Super::RebuildWidget();
		if (CustomOnClick.IsBound())
		{
			MyButton->SetOnClicked(CustomOnClick);
		}
		return res;
	}

private:
	FOnClicked CustomOnClick;

public:
	void SetOnClicked(FOnClicked clickFunc)
	{
		CustomOnClick = clickFunc;
		if (MyButton.IsValid())
		{
			MyButton->SetOnClicked(clickFunc);
		}
	}
};

UCLASS(config=Game)
class AMinigameSetMenu : public AMinigameMenu
{
	GENERATED_UCLASS_BODY()

public:
	UPROPERTY(EditAnywhere)
		TSubclassOf<UUserWidget> MenuWidgetClass;
	UPROPERTY(EditAnywhere)
		TSubclassOf<UUserWidget> ScoreWidgetClass;
	UPROPERTY(EditAnywhere)
		TSubclassOf<UUserWidget> ResultWidgetClass;

public:
	virtual void OnBegin() override;
	virtual void OnShowMenu(bool bShow) override;
	virtual void OnChangeScore(FName name, FText desc, FString score_str) override;
	virtual void OnResult(bool bWin) override;

private:
	void InitMenuWidget();
	void InitScoreWidget();
	void InitResultWidget();

private:
	UPROPERTY(Transient)
		UUserWidget* MenuWidget;
	UPROPERTY(Transient)
		UUserWidget* ScoreWidget;
	UPROPERTY(Transient)
		UUserWidget* ResultWidget;
};