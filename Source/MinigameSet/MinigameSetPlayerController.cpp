// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "MinigameSetPlayerController.h"

AMinigameSetPlayerController::AMinigameSetPlayerController()
{
	bShowMouseCursor = true;
	bEnableClickEvents = true;
	bEnableTouchEvents = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
}
