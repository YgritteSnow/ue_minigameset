#pragma once

#include "CoreMinimal.h"
#include "MinigameLogic/Public/Minigame_Hanoi.h"
#include "MinigameResource_Hanoi.generated.h"

/** A block that can be clicked */
UCLASS(minimalapi)
class AMinigame_Hanoi_Brick_3D : public AMinigame_Hanoi_Brick
{
	GENERATED_BODY()
public:
	virtual void MoveToPos(int dsc_col, int dsc_idx, float time) override;
	virtual void OnChangeIndex() override;

	void RefreshHighlight();
	void SetIsInside(bool b);
	void SetIsPressing(bool b);

public:
	UPROPERTY(Category = Minigame, EditAnywhere, BlueprintReadWrite)
		float BrickInsideOffset;
	UPROPERTY(Category = Minigame, EditAnywhere, BlueprintReadWrite)
		float LengthParam = 0.1;

	/** Dummy root component */
	UPROPERTY(Category = Block, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class USceneComponent* DummyRoot;

	/** StaticMesh component for the clickable block */
	UPROPERTY(Category = Block, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class UStaticMeshComponent* BlockMesh;


public:
	virtual void Tick(float DeltaSeconds) override;

private:
	bool bIsMoving;
	FVector TargetMovingPos;
	float TimeRemain;
	void MoveToPosInner(const FVector& pos);

	bool bIsPressing;
	bool bIsInside;

public:
	AMinigame_Hanoi_Brick_3D(const FObjectInitializer& initializer);


	/** Pointer to white material used on the focused block */
	UPROPERTY()
		class UMaterial* BaseMaterial;

	/** Pointer to blue material used on inactive blocks */
	UPROPERTY()
		class UMaterialInstance* BlueMaterial;

	/** Pointer to orange material used on active blocks */
	UPROPERTY()
		class UMaterialInstance* OrangeMaterial;

	/** Pointer to blue material used on inactive blocks */
	UPROPERTY()
		class UMaterialInstanceDynamic* ColorMaterial;

	UFUNCTION()
	void HandlePress(UPrimitiveComponent* ClickedComp, FKey ButtonClicked);
	UFUNCTION()
	void HandleRelease(UPrimitiveComponent* ClickedComp, FKey ButtonClicked);
	UFUNCTION()
	void HandleLeave(UPrimitiveComponent* ClickedComp);
	UFUNCTION()
	void HandleEnter(UPrimitiveComponent* ClickedComp);

	/** Handle the block being touched  */
	UFUNCTION()
		void OnFingerPressedBlock(ETouchIndex::Type FingerIndex, UPrimitiveComponent* TouchedComponent);

public:
	/** Returns DummyRoot subobject **/
	FORCEINLINE class USceneComponent* GetDummyRoot() const { return DummyRoot; }
	/** Returns BlockMesh subobject **/
	FORCEINLINE class UStaticMeshComponent* GetBlockMesh() const { return BlockMesh; }
};

/** A block that can be clicked */
UCLASS(minimalapi)
class AMinigame_Hanoi_Colume_3D : public AMinigame_Hanoi_Colume
{
	GENERATED_BODY()
public:
	virtual void SetBrickInside(bool bLight) override;
	virtual void SetSelect(bool bLight) override;

	virtual void OnChangeIndex() override { SetActorLocation(FVector(0, GetIndex() * (ColumeWidth+ColumePadding) - PosOffset, 0.f)); };
	virtual bool IsPosInside(FVector2D pos) override;

private:
	bool bIsInside;
	bool bIsSelecting;
	void RefreshHighlight();

public:
	/** Dummy root component */
	UPROPERTY(Category = Block, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class USceneComponent* DummyRoot;

	/** StaticMesh component for the clickable block */
	UPROPERTY(Category = Block, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class UStaticMeshComponent* BlockMesh;

public:

public:
	AMinigame_Hanoi_Colume_3D(const FObjectInitializer& initializer);


	/** Pointer to white material used on the focused block */
	UPROPERTY()
		class UMaterial* BaseMaterial;

	/** Pointer to blue material used on inactive blocks */
	UPROPERTY()
		class UMaterialInstance* BlueMaterial;

	/** Pointer to orange material used on active blocks */
	UPROPERTY()
		class UMaterialInstance* OrangeMaterial;

	/** Pointer to blue material used on inactive blocks */
	UPROPERTY()
		class UMaterialInstanceDynamic* ColorMaterial;

	UFUNCTION()
	void HandlePress(UPrimitiveComponent* ClickedComp, FKey ButtonClicked);

	/** Handle the block being touched  */
	UFUNCTION()
		void OnFingerPressedBlock(ETouchIndex::Type FingerIndex, UPrimitiveComponent* TouchedComponent);

public:
	/** Returns DummyRoot subobject **/
	FORCEINLINE class USceneComponent* GetDummyRoot() const { return DummyRoot; }
	/** Returns BlockMesh subobject **/
	FORCEINLINE class UStaticMeshComponent* GetBlockMesh() const { return BlockMesh; }
};



