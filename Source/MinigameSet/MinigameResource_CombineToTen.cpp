
#include "MinigameResource_CombineToTen.h"
#include "MinigameSetBlock.h"
#include "MinigameSetBlockGrid.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/StaticMesh.h"
#include "Materials/MaterialInstance.h"
#include "Components/TextRenderComponent.h"

extern FLogCategoryLogJJ LogJJ;

AMinigame_CombineToTen_Brick_3D::AMinigame_CombineToTen_Brick_3D()
	: Super()
	, bIsMoving(false)
	, TargetMovingPos(FVector::ZeroVector)
	, TimeRemain(0)
{
	PrimaryActorTick.bCanEverTick = true;

	// Structure to hold one-time initialization
	struct FConstructorStatics
	{
		ConstructorHelpers::FObjectFinderOptional<UStaticMesh> PlaneMesh;
		ConstructorHelpers::FObjectFinderOptional<UMaterial> BaseMaterial;
		ConstructorHelpers::FObjectFinderOptional<UMaterialInstance> BlueMaterial;
		ConstructorHelpers::FObjectFinderOptional<UMaterialInstance> OrangeMaterial;
		FConstructorStatics()
			: PlaneMesh(TEXT("/Game/Puzzle/Meshes/PuzzleCube.PuzzleCube"))
			, BaseMaterial(TEXT("/Game/Puzzle/Meshes/BaseMaterial.BaseMaterial"))
			, BlueMaterial(TEXT("/Game/Puzzle/Meshes/BlueMaterial.BlueMaterial"))
			, OrangeMaterial(TEXT("/Game/Puzzle/Meshes/OrangeMaterial.OrangeMaterial"))
		{
		}
	};
	static FConstructorStatics ConstructorStatics;

	// Create dummy root scene component
	DummyRoot = CreateDefaultSubobject<USceneComponent>(TEXT("Dummy0"));
	RootComponent = DummyRoot;

	// Create static mesh component
	BlockMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BlockMesh0"));
	BlockMesh->SetStaticMesh(ConstructorStatics.PlaneMesh.Get());
	BlockMesh->SetRelativeScale3D(FVector(1.f, 1.f, 0.25f));
	BlockMesh->SetRelativeLocation(FVector(0.f, 0.f, 25.f));
	BlockMesh->SetMaterial(0, ConstructorStatics.BlueMaterial.Get());
	BlockMesh->SetupAttachment(DummyRoot);
	BlockMesh->OnClicked.AddDynamic(this, &AMinigame_CombineToTen_Brick_3D::BlockClicked);
	BlockMesh->OnInputTouchBegin.AddDynamic(this, &AMinigame_CombineToTen_Brick_3D::OnFingerPressedBlock);

	Text = CreateDefaultSubobject<UTextRenderComponent>(TEXT("Text"));
	Text->SetupAttachment(BlockMesh);
	Text->WorldSize = 200.f;
	Text->SetRelativeRotation(FRotator(0, 90, 0));
	Text->SetRelativeLocation(FVector(60.f, 0.f, 0.f));

	// Save a pointer to the orange material
	BaseMaterial = ConstructorStatics.BaseMaterial.Get();
	BlueMaterial = ConstructorStatics.BlueMaterial.Get();
	OrangeMaterial = ConstructorStatics.OrangeMaterial.Get();
}

void AMinigame_CombineToTen_Brick_3D::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (bIsMoving)
	{
		if (TimeRemain <= 0)
		{
			bIsMoving = false;
			MoveToPosInner(TargetMovingPos);

			//BlockMesh->SetMaterial(0, BaseMaterial);
		}
		else
		{
			MoveToPosInner(FMath::Lerp(GetActorLocation(), TargetMovingPos, DeltaSeconds/TimeRemain));
		}
		TimeRemain -= DeltaSeconds;
	}
}

void AMinigame_CombineToTen_Brick_3D::MoveToPos(int x, int y, float time)
{
	const float XOffset = -y * (BrickSize + BrickPadding) + PosOffset;
	const float YOffset = x * (BrickSize + BrickPadding) - PosOffset;

	TargetMovingPos = FVector(XOffset, YOffset, 0.f);

	if (FMath::IsNearlyEqual(time, 0))
	{
		bIsMoving = false;
		MoveToPosInner(TargetMovingPos);
	}
	else
	{
		bIsMoving = true;
		TimeRemain = time;
	}
}
void AMinigame_CombineToTen_Brick_3D::MoveToPosInner(const FVector& pos)
{
	SetActorLocation(pos);
}

void AMinigame_CombineToTen_Brick_3D::ShowNumber(int value)
{
	Text->SetText(FText::FromString(FString::FromInt(value)));
}

void AMinigame_CombineToTen_Brick_3D::ShowSelected(bool bSelected)
{
	if (bSelected)
	{
		BlockMesh->SetMaterial(0, OrangeMaterial);
	}
	else
	{
		BlockMesh->SetMaterial(0, BlueMaterial);
	}
}

void AMinigame_CombineToTen_Brick_3D::BlockClicked(UPrimitiveComponent* ClickedComp, FKey ButtonClicked)
{
	UE_LOG(LogJJ, Warning, TEXT("BlockClicked: %s"), *ButtonClicked.ToString());
	HandleClicked();
}


void AMinigame_CombineToTen_Brick_3D::OnFingerPressedBlock(ETouchIndex::Type FingerIndex, UPrimitiveComponent* TouchedComponent)
{
	UE_LOG(LogJJ, Warning, TEXT("OnFingerPressedBlock: %d"), (int)FingerIndex);
	HandleClicked();
}

void AMinigame_CombineToTen_Brick_3D::HandleClicked()
{
	OnMousePress.ExecuteIfBound();
}

void AMinigame_CombineToTen_Brick_3D::Highlight(bool bOn)
{
	// Do not highlight if the block has already been activated.
	if (bIsActive)
	{
		return;
	}

	if (bOn)
	{
		BlockMesh->SetMaterial(0, BaseMaterial);
	}
	else
	{
		BlockMesh->SetMaterial(0, BlueMaterial);
	}
}
