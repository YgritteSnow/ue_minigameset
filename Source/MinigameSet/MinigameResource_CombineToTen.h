#pragma once

#include "CoreMinimal.h"
#include "MinigameLogic/Public/Minigame_CombineToTen.h"
#include "MinigameResource_CombineToTen.generated.h"

/** A block that can be clicked */
UCLASS(minimalapi)
class AMinigame_CombineToTen_Brick_3D : public AMinigame_CombineToTen_Brick
{
	GENERATED_BODY()

	/** Dummy root component */
	UPROPERTY(Category = Block, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class USceneComponent* DummyRoot;

	/** StaticMesh component for the clickable block */
	UPROPERTY(Category = Block, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class UStaticMeshComponent* BlockMesh;

	UPROPERTY(Category = Block, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class UTextRenderComponent* Text;

public:
	virtual void Tick(float DeltaSeconds) override;

public:
	virtual void MoveToPos(int x, int y, float time) override;
	virtual void ShowNumber(int value) override;
	virtual void ShowSelected(bool bSelected) override;
private:
	bool bIsMoving;
	FVector TargetMovingPos;
	float TimeRemain;
	void MoveToPosInner(const FVector& pos);

public:
	AMinigame_CombineToTen_Brick_3D();

	/** Are we currently active? */
	bool bIsActive;

	/** Pointer to white material used on the focused block */
	UPROPERTY()
		class UMaterial* BaseMaterial;

	/** Pointer to blue material used on inactive blocks */
	UPROPERTY()
		class UMaterialInstance* BlueMaterial;

	/** Pointer to orange material used on active blocks */
	UPROPERTY()
		class UMaterialInstance* OrangeMaterial;

	/** Handle the block being clicked */
	UFUNCTION()
		void BlockClicked(UPrimitiveComponent* ClickedComp, FKey ButtonClicked);

	/** Handle the block being touched  */
	UFUNCTION()
		void OnFingerPressedBlock(ETouchIndex::Type FingerIndex, UPrimitiveComponent* TouchedComponent);

	void HandleClicked();

	void Highlight(bool bOn);

public:
	/** Returns DummyRoot subobject **/
	FORCEINLINE class USceneComponent* GetDummyRoot() const { return DummyRoot; }
	/** Returns BlockMesh subobject **/
	FORCEINLINE class UStaticMeshComponent* GetBlockMesh() const { return BlockMesh; }
};



