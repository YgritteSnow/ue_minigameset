// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "MinigameSetGameMode.h"
#include "MinigameSetPlayerController.h"
#include "MinigameSetPawn.h"

AMinigameSetGameMode::AMinigameSetGameMode()
{
	// no pawn by default
	DefaultPawnClass = AMinigameSetPawn::StaticClass();
	// use our own player controller class
	PlayerControllerClass = AMinigameSetPlayerController::StaticClass();
}
