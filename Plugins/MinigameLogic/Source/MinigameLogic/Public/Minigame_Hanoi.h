#pragma once

#include "MinigameMenuInterface.h"
#include "Minigame_Hanoi.generated.h"

DECLARE_DELEGATE(FOnMouseClick);
DECLARE_DELEGATE(FOnMouseDrag);
DECLARE_DELEGATE(FOnMouseDrop);
DECLARE_DELEGATE_OneParam(FOnDragMove, FVector2D /*MousePos*/);

class AMinigame_Hanoi;

/* Resource Structs */
UCLASS(BlueprintType)
class MINIGAMELOGIC_API AMinigame_Hanoi_Brick : public AActor
{
	GENERATED_UCLASS_BODY()

public:
	UPROPERTY(Category = Minigame, EditAnywhere, BlueprintReadWrite)
		float BrickWidth;

	float ColumeWidth;
	FVector2D PosOffset;
public:
	FOnMouseDrag OnMouseDrag;
	FOnMouseDrop OnMouseDrop;
	FOnDragMove OnDragMove;

	virtual void MoveToPos(int dsc_col, int dsc_idx, float time) {};
	virtual void OnChangeIndex() {};
	
	int GetIndex() { return Index; }

private:
	friend AMinigame_Hanoi;
	void SetIndex(int length) { *const_cast<int*>(&Index) = length; OnChangeIndex(); };
	int Index;
};

UCLASS(BlueprintType)
class MINIGAMELOGIC_API AMinigame_Hanoi_Colume : public AActor
{
	GENERATED_UCLASS_BODY()
public:
	UPROPERTY(Category = Minigame, EditAnywhere, BlueprintReadWrite)
		float ColumeWidth;
	UPROPERTY(Category = Minigame, EditAnywhere, BlueprintReadWrite)
		float ColumePadding;
	UPROPERTY(Category = Minigame, EditAnywhere, BlueprintReadWrite)
		float ColumeHeight;

	float PosOffset;

	FOnMouseClick OnMouseClick;

	virtual void SetBrickInside(bool bLight) {};
	virtual void SetSelect(bool bLight) {};
	virtual void OnChangeIndex() {};
	virtual bool IsPosInside(FVector2D pos) { return false; };

	int GetIndex() { return Index; }

private:
	friend AMinigame_Hanoi;
	void SetIndex(int length) { *const_cast<int*>(&Index) = length; OnChangeIndex(); };
	int Index;
};


/* Game option Structs */
class FMinigame_Hanoi_Options : public IMinigameMenuOption
{
public:
	virtual void InitDefault() override;
};

class FMinigame_Hanoi_Score : public IMinigameScore
{
public:
	virtual void InitDefault() override;
};

/* Game logic Structs */
UCLASS(BlueprintType)
class MINIGAMELOGIC_API AMinigame_Hanoi : public AMinigameMenuEntry
{
	GENERATED_UCLASS_BODY()
public:
	UPROPERTY(EditAnywhere)
		TSubclassOf<AMinigame_Hanoi_Brick> BrickResourceTempl;

	UPROPERTY(EditAnywhere)
		TSubclassOf<AMinigame_Hanoi_Colume> ColumeResourceTempl;

public:
	/* ~(bgn) Derived from AMinigameMenuEntry ~ */
	virtual FName GetGameName() override
	{
		return FName("Hanoi");
	}

	virtual void OnStart() override;
	virtual void OnRestart() override;
	virtual void OnEnd() override;
	/* ~(end) Derived from AMinigameMenuEntry ~ */

private:
	enum MoveDir
	{
		Up,
		Down,
		Left,
		Right,
	};
	void SyncBrickStateImmediate();
	void SelectFocusColume(int columeIdx, bool forceFocus = false);
	int FindBrickColume(int brickIdx);
	void SetMovingInsideColume(int columeIdx, bool bLight);
	void SetManuallyMoving(int movingBrickIdx);
	int FindPosInsideColume(FVector2D pos);

	void FindAndRemoveBrick(int brickIdx);
	void AddBrickToColume(int brickIdx, int columeIdx, float time=0.f);

	void ClearGameState();
	int CheckCanMove(int src_col, int dst_col);

private:
	int ColumeCount;
	float Speed;
	int BrickCount;

	UPROPERTY(Transient)
		TArray<AMinigame_Hanoi_Brick*> BrickObjects;
	UPROPERTY(Transient)
		TArray<AMinigame_Hanoi_Colume*> ColumeObjects;
private:
	int StepScore;
	int FocusColumeIdx;
	TArray<TArray<int>> BrickStates;
	int MovingBrickIdx;
	int MovingInsideColumeIdx;
};