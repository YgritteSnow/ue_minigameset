#pragma once

#include "MinigameMenuInterface.h"
#include "Minigame_CombineToTen.generated.h"

DECLARE_DELEGATE(FOnMousePress);

class AMinigame_CombineToTen;

/* Resource Structs */
UCLASS(BlueprintType)
class MINIGAMELOGIC_API AMinigame_CombineToTen_Brick : public AActor
{
	GENERATED_UCLASS_BODY()
public:
	FOnMousePress OnMousePress;

	virtual void MoveToPos(int x, int y, float time) {};
	virtual void ShowNumber(int value) {};
	virtual void ShowSelected(bool bSelected) {};

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float BrickSize;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float BrickPadding;
protected:
	friend AMinigame_CombineToTen;
	float PosOffset;
};

USTRUCT(BlueprintType)
struct FMinigame_CombineToTen_Resource
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, Category = "Minigame")
		TSubclassOf<AMinigame_CombineToTen_Brick> BrickResourceTempl;
};

/* Game option Structs */
class FMinigame_CombineToTen_Options : public IMinigameMenuOption
{
public:
	virtual void InitDefault() override;
};

class FMinigame_CombineToTen_Score : public IMinigameScore
{
public:
	virtual void InitDefault() override;
};

/* Game logic Structs */
UCLASS(BlueprintType)
class MINIGAMELOGIC_API AMinigame_CombineToTen : public AMinigameMenuEntry
{
	GENERATED_UCLASS_BODY()
public:
	UPROPERTY(EditAnywhere)
		FMinigame_CombineToTen_Resource GameResource;

public:
	/* ~(bgn) Derived from AMinigameMenuEntry ~ */
	virtual FName GetGameName() override
	{
		return FName("CombineToTen");
	}

	virtual void OnStart() override;
	virtual void OnRestart() override;
	virtual void OnEnd() override;
	/* ~(end) Derived from AMinigameMenuEntry ~ */

private:
	enum MoveDir
	{
		Up,
		Down,
		Left,
		Right,
	};
	void SyncBrickStateImmediate();
	void OnClickBrick(int brickIdx);
	void ClearGameState();
	void CalcTargetBrick(MoveDir dir);
	int GetRandomNumber() { return 1+(FMath::Rand() % (Goal/3)); };
	void MoveToPos(int brick_idx, int move_pos, float time);
	void MoveToPos(int brick_idx, int move_pos_x, int move_pos_y, float time);

private:
	int Dimension;
	int DimensionSquare;
	int Goal;
	float Speed;
	UPROPERTY(Transient)
		TArray<AMinigame_CombineToTen_Brick*> BrickObjects;

private:
	int BrickCountScore;
	int BrickNumberScore;
	int FocusNumber;
	TArray<int> BrickStates;
	TArray<bool> SelectedBrickMasks;
};