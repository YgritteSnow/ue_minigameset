// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Modules/ModuleManager.h"
#include "GameFramework/Actor.h"
#include "MinigameMenuInterface.generated.h"

#define MINIGAME_OPTION_DECALREPARAM(paramType) \
public: \
	bool SetParam_##paramType(FName paramName, paramType paramValue){ \
		if(paramMap_##paramType.Contains(paramName)) \
		{ \
			paramMap_##paramType[paramName] = paramValue; \
			return true; \
		} \
		else \
		{ \
			return false; \
		} \
	}; \
	paramType GetParam_##paramType(FName paramName) { \
		return paramMap_##paramType.FindChecked(paramName); \
	}; \
protected: \
	void InitParam_##paramType(FName paramName, FText showName, paramType paramValue){ \
		paramMap_##paramType.Add(paramName, paramValue); \
		paramMap_name.Add(paramName, showName); \
	}; \
private: \
	TMap<FName, paramType> paramMap_##paramType;

#define MINIGAME_SCORE_DECALREPARAM(paramType) \
public: \
	bool SetParam_##paramType(FName paramName, paramType paramValue){ \
		if(paramMap_##paramType.Contains(paramName)) \
		{ \
			paramMap_##paramType[paramName] = paramValue; \
			if(OwnerMenu) \
			{ \
				OwnerMenu->ChangeScore(paramName, paramMap_name[paramName], paramMap_##paramType[paramName]); \
			} \
			return true; \
		} \
		else \
		{ \
			return false; \
		} \
	}; \
	paramType GetParam_##paramType(FName paramName) { \
		return paramMap_##paramType.FindChecked(paramName); \
	}; \
protected: \
	void InitParam_##paramType(FName paramName, FText showName, paramType paramValue){ \
		paramMap_##paramType.Add(paramName, paramValue); \
		paramMap_name.Add(paramName, showName); \
		if(OwnerMenu) \
		{ \
			OwnerMenu->ChangeScore(paramName, paramMap_name[paramName], paramMap_##paramType[paramName]); \
		} \
	}; \
private: \
	TMap<FName, paramType> paramMap_##paramType;

class MINIGAMELOGIC_API IMinigameMenuOption
{
public:
	virtual ~IMinigameMenuOption() {}
	virtual void InitDefault() = 0;

private:
	TMap<FName, FText> paramMap_name;

	MINIGAME_OPTION_DECALREPARAM(int)
	MINIGAME_OPTION_DECALREPARAM(float)
	MINIGAME_OPTION_DECALREPARAM(FString)
	MINIGAME_OPTION_DECALREPARAM(bool)
};

class AMinigameMenu;
class IMinigameScore;
UCLASS(BlueprintType)
class MINIGAMELOGIC_API AMinigameMenuEntry : public AActor
{
	GENERATED_UCLASS_BODY()

public:
	~AMinigameMenuEntry();
	virtual FName GetGameName()
	{
		return FName("DummyGame");
	}

	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	void DoRestart() { OnRestart(); }
	virtual int DoCheckIfEnd() { return 0; };

public:
	void SetOwnerMenu(AMinigameMenu* MenuPtr);
private:
	virtual void OnStart() {}
	virtual void OnRestart() {};
	virtual void OnEnd() {}
	virtual void OnGameMouseTouchStart(FVector2D mousePos) {}
	virtual void OnGameMouseTouchMove(FVector2D mousePos) {}
	virtual void OnGameMouseTouchEnd(FVector2D mousePos) {}

protected:
	AMinigameMenu* OwnerMenu;

protected:
	IMinigameMenuOption* GameOption;
	IMinigameScore* GameScore;
};

UCLASS(config = Game)
class MINIGAMELOGIC_API AMinigameMenu : public AActor
{
	GENERATED_UCLASS_BODY()

public:
	UPROPERTY(EditAnywhere)
		TArray<TSubclassOf< AMinigameMenuEntry>> GameEntryClasses;

public:
	virtual ~AMinigameMenu() {}

public:
	void EndCurrentGame()
	{
		if(CurrentGameEntryInst)
			CurrentGameEntryInst->Destroy();
	}
	void RestartCurrentGame()
	{
		if (CurrentGameEntryInst)
			CurrentGameEntryInst->DoRestart();
	};
	virtual void BeginPlay() override {
		Super::BeginPlay();
		OnBegin();
	}
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override {
		Super::EndPlay(EndPlayReason);
		OnEnd();

		CurrentGameEntryInst = nullptr;
	}

public:
	virtual void OnBegin() {};
	virtual void OnResult(bool bWin) {};
	virtual void OnEnd() {};
	virtual void OnShowMenu(bool bShow) {};
	virtual void OnChangeScore(FName name, FText desc, FString score_str) {};

public:
	void GameEnd(int result)
	{
		if(result != 0)
		{
			OnResult(result > 0);
		}
	}
	void ShowMenu(bool bShow)
	{
		OnShowMenu(bShow);
	}
	void ChangeScore(FName name, FText desc, FString score_str)
	{
		OnChangeScore(name, desc, score_str);
	}
	void ChangeScore(FName name, FText desc, int score_str)
	{
		OnChangeScore(name, desc, FString::FromInt(score_str));
	}
	void ChangeScore(FName name, FText desc, float score_str)
	{
		OnChangeScore(name, desc, FString::Printf(TEXT(".2f"), score_str));
	}
protected:
	void DoSelectGame(int index)
	{
		CurrentGameEntryInst = Cast<AMinigameMenuEntry>(GetWorld()->SpawnActor(GameEntryClasses[index], &FVector::ZeroVector, &FRotator::ZeroRotator));
		CurrentGameEntryInst->SetOwnerMenu(this);

		ShowMenu(false);
	}
private:
	UPROPERTY(Transient)
		AMinigameMenuEntry * CurrentGameEntryInst;
};

class MINIGAMELOGIC_API IMinigameScore
{
public:
	virtual ~IMinigameScore() {}
	void SetOwnerMenu(AMinigameMenu* owner)
	{
		OwnerMenu = owner;
		if (OwnerMenu)
		{
			for (auto It = paramMap_int.CreateConstIterator(); It; ++It)
			{
				OwnerMenu->ChangeScore(It->Key, paramMap_name[It->Key], It->Value);
			}
			for (auto It = paramMap_float.CreateConstIterator(); It; ++It)
			{
				OwnerMenu->ChangeScore(It->Key, paramMap_name[It->Key], It->Value);
			}
		}
	}
	virtual void InitDefault() = 0;

private:
	TMap<FName, FText> paramMap_name;
	AMinigameMenu* OwnerMenu;

	MINIGAME_SCORE_DECALREPARAM(int)
	MINIGAME_SCORE_DECALREPARAM(float)
};