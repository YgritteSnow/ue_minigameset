// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "Minigame_Hanoi.h"
#include "Internationalization/Internationalization.h"
#include "Engine/Classes/GameFramework/PlayerController.h"

#define LOCTEXT_NAMESPACE "Minigame_Hanoi"

static const FName Hanoi_Option_ColumeCount("Dimision");
static const FName Hanoi_Option_Speed("Speed");
static const FName Hanoi_Option_BrickCount("BrickCount");
static const FName Hanoi_Score_StepCount("StepCount");
static const int Hanoi_INIT_NUMBER = 0;
static const int Hanoi_INVALID_NUMBER = -1;

DEFINE_LOG_CATEGORY_STATIC(LogHanoi, Warning, All);

AMinigame_Hanoi_Brick::AMinigame_Hanoi_Brick(const FObjectInitializer& initializer)
	: Super(initializer)
	, Index(Hanoi_INVALID_NUMBER)
{}

AMinigame_Hanoi_Colume::AMinigame_Hanoi_Colume(const FObjectInitializer& initializer)
	: Super(initializer)
	, Index(Hanoi_INVALID_NUMBER)
{}

void FMinigame_Hanoi_Options::InitDefault()
{
	InitParam_int(Hanoi_Option_ColumeCount, LOCTEXT("Hanoi", "ColumeCount"), 3);
	InitParam_float(Hanoi_Option_Speed, LOCTEXT("Hanoi", "Speed"), 0.2);
	InitParam_int(Hanoi_Option_BrickCount, LOCTEXT("Hanoi", "BrickCount"), 3);
}

void FMinigame_Hanoi_Score::InitDefault()
{
	InitParam_int(Hanoi_Score_StepCount, LOCTEXT("Hanoi", "StepCount"), 0);
}

AMinigame_Hanoi::AMinigame_Hanoi(const FObjectInitializer& initializer)
	: Super(initializer)
	, Speed(1.f)
	, StepScore(0)
	, FocusColumeIdx(Hanoi_INVALID_NUMBER)
{
	GameOption = new FMinigame_Hanoi_Options();
	GameScore = new FMinigame_Hanoi_Score();
	GameOption->InitDefault();
	GameScore->InitDefault();
}

void AMinigame_Hanoi::OnStart()
{
	APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
	PlayerController->bEnableMouseOverEvents = true;

	ColumeCount = GameOption->GetParam_int(Hanoi_Option_ColumeCount);
	Speed = GameOption->GetParam_float(Hanoi_Option_Speed);
	BrickCount = GameOption->GetParam_int(Hanoi_Option_BrickCount);

	ClearGameState();

	float colume_width = 0.f;
	float colume_padding = 0.f;
	float colume_height = 0.f;
	float colume_offset = 0.f;

	ColumeObjects.Reset();
	ColumeObjects.Reserve(ColumeCount);
	for (int i = 0; i < ColumeCount; ++i)
	{
		AMinigame_Hanoi_Colume* colume = Cast<AMinigame_Hanoi_Colume>(GetWorld()->SpawnActor(ColumeResourceTempl, &FVector::ZeroVector, &FRotator::ZeroRotator));
		check(colume != nullptr);

		colume->OnMouseClick = FOnMouseClick::CreateLambda([this, i]() {
			SelectFocusColume(i);
		});

		colume_offset = (ColumeCount - 1) * 0.5*(colume->ColumeWidth + colume->ColumePadding);// +colume->ColumeWidth / 2;
		colume_padding = colume->ColumePadding;
		colume->PosOffset = colume_offset;
		colume->SetIndex(i);
		colume_width = colume->ColumeWidth;
		colume_height = colume->ColumeHeight;
		ColumeObjects.Add(colume);
	}

	BrickObjects.Reset();
	BrickObjects.Reserve(BrickCount);
	for (int i = 0; i < BrickCount; ++i)
	{
		AMinigame_Hanoi_Brick* brick = Cast<AMinigame_Hanoi_Brick>(GetWorld()->SpawnActor(BrickResourceTempl, &FVector::ZeroVector, &FRotator::ZeroRotator));
		check(brick != nullptr);

		brick->ColumeWidth = colume_width + colume_padding;
		brick->PosOffset = FVector2D(colume_offset, colume_height / 2 - brick->BrickWidth / 2);

		brick->OnMouseDrag = FOnMouseDrag::CreateLambda([this, i]() {
			SetManuallyMoving(i);
			MovingInsideColumeIdx = FindBrickColume(i);
			SelectFocusColume(MovingInsideColumeIdx, true);
		});
		brick->OnMouseDrop = FOnMouseDrop::CreateLambda([this, i]() {
			SetManuallyMoving(Hanoi_INVALID_NUMBER);
			SelectFocusColume(FindBrickColume(i));
			MovingInsideColumeIdx = Hanoi_INVALID_NUMBER;
		});
		brick->OnDragMove = FOnDragMove::CreateLambda([this, i](FVector2D curPos) {
			int insideColumeIdx = FindPosInsideColume(curPos);
			if (insideColumeIdx != MovingInsideColumeIdx)
			{
				SetMovingInsideColume(insideColumeIdx, true);
				SetMovingInsideColume(MovingInsideColumeIdx, false);
				MovingInsideColumeIdx = insideColumeIdx;
			}
		});

		brick->SetIndex(i);
		BrickObjects.Add(brick);
	}
	SyncBrickStateImmediate();
}

void AMinigame_Hanoi::OnRestart()
{
	ClearGameState();
	SyncBrickStateImmediate();
}

void AMinigame_Hanoi::OnEnd()
{
	ClearGameState();
	APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
	if(PlayerController)
		PlayerController->bEnableMouseOverEvents = false;

	for (int i = 0; i < BrickObjects.Num(); ++i)
	{
		BrickObjects[i]->Destroy();
	}
	BrickObjects.Reset();

	for (int i = 0; i < ColumeObjects.Num(); ++i)
	{
		ColumeObjects[i]->Destroy();
	}
	ColumeObjects.Reset();
}

void AMinigame_Hanoi::FindAndRemoveBrick(int brickIdx)
{
	for (auto It_Arr = BrickStates.CreateIterator(); It_Arr; ++It_Arr)
	{
		It_Arr->RemoveAll([brickIdx](int t) {
			return t == brickIdx;
		});
	}
}
void AMinigame_Hanoi::AddBrickToColume(int brickIdx, int columeIdx, float time)
{
	BrickStates[columeIdx].Add(brickIdx);
	BrickObjects[brickIdx]->MoveToPos(columeIdx, BrickStates[columeIdx].Num() - 1, time);

	if(columeIdx != 0 && BrickStates[columeIdx].Num() == BrickCount)
	{
		OwnerMenu->GameEnd(1);
	}
}

void AMinigame_Hanoi::ClearGameState()
{
	FocusColumeIdx = Hanoi_INVALID_NUMBER;
	GameScore->InitDefault();
	StepScore = 0;
	BrickStates.Reset();
	BrickStates.Reserve(ColumeCount);
	for (int i = 0; i < ColumeCount; ++i)
	{
		BrickStates.Add(TArray<int>());
		BrickStates[i].Reserve(BrickCount);
	}

	for (int i = 0; i < BrickCount; ++i)
	{
		BrickStates[0].Add(BrickCount-i-1);
	}

	SyncBrickStateImmediate();
}

void AMinigame_Hanoi::SyncBrickStateImmediate()
{
	if (BrickObjects.Num() == 0)
		return;

	for (auto It_Colume = BrickStates.CreateConstIterator(); It_Colume; ++It_Colume)
	{
		for (auto It_Brick = It_Colume->CreateConstIterator(); It_Brick; ++It_Brick)
		{
			if (BrickObjects[*It_Brick])
			{
				BrickObjects[*It_Brick]->MoveToPos(It_Colume.GetIndex(), It_Brick.GetIndex(), 0);
			}
		}
	}
}

int AMinigame_Hanoi::CheckCanMove(int src_col, int dst_col)
{
	if (BrickStates[src_col].Num() <= 0)
	{
		return Hanoi_INVALID_NUMBER;
	}
	int src_brick_idx = BrickStates[src_col][BrickStates[src_col].Num() - 1];

	if (BrickStates[dst_col].Num() == 0)
	{
		return src_brick_idx;
	}

	int dst_brick_idx = BrickStates[dst_col][BrickStates[dst_col].Num() - 1];
	return (src_brick_idx <= dst_brick_idx) ? src_brick_idx : Hanoi_INVALID_NUMBER;
}

int AMinigame_Hanoi::FindBrickColume(int brickIdx)
{
	for (auto It_Colume = BrickStates.CreateIterator(); It_Colume; ++It_Colume)
	{
		for (auto It_Brick = (*It_Colume).CreateIterator(); It_Brick; ++It_Brick)
		{
			if (*It_Brick == brickIdx)
			{
				return It_Colume.GetIndex();
			}
		}
	}
	ensureMsgf(false, TEXT("Invalid brickIdx !"));
	return Hanoi_INVALID_NUMBER;
}

int AMinigame_Hanoi::FindPosInsideColume(FVector2D pos)
{
	for (auto It_Colume = ColumeObjects.CreateIterator(); It_Colume; ++It_Colume)
	{
		if ((*It_Colume)->IsPosInside(pos))
		{
			return It_Colume.GetIndex();
		}
	}
	return Hanoi_INVALID_NUMBER;
}

void AMinigame_Hanoi::SetManuallyMoving(int movingBrickIdx)
{
	MovingBrickIdx = movingBrickIdx;
}

void AMinigame_Hanoi::SetMovingInsideColume(int columeIdx, bool bLight)
{
	ColumeObjects[columeIdx]->SetBrickInside(bLight);
}

void AMinigame_Hanoi::SelectFocusColume(int columeIdx, bool forceFocus)
{
	UE_LOG(LogHanoi, Warning, TEXT("SelectColume: %d->%d, %d"), FocusColumeIdx, columeIdx, forceFocus?1:0)
	if (FocusColumeIdx == columeIdx)
	{
		if (!forceFocus)
		{
			FocusColumeIdx = Hanoi_INVALID_NUMBER;
			ColumeObjects[columeIdx]->SetSelect(false);
		}
	}
	else if (FocusColumeIdx == Hanoi_INVALID_NUMBER)
	{
		if (BrickStates[columeIdx].Num() > 0)
		{
			FocusColumeIdx = columeIdx;
			ColumeObjects[columeIdx]->SetSelect(true);
		}
	}
	else
	{
		if (forceFocus)
		{
			ColumeObjects[FocusColumeIdx]->SetSelect(false);
			ColumeObjects[columeIdx]->SetSelect(true);
			FocusColumeIdx = columeIdx;
		}
		else
		{
			int brick_idx = CheckCanMove(FocusColumeIdx, columeIdx);
			if (brick_idx != Hanoi_INVALID_NUMBER)
			{
				++StepScore;
				GameScore->SetParam_int(Hanoi_Score_StepCount, StepScore);

				ColumeObjects[FocusColumeIdx]->SetSelect(false);
				ColumeObjects[columeIdx]->SetSelect(false);

				FindAndRemoveBrick(brick_idx);
				AddBrickToColume(brick_idx, columeIdx, Speed);

				FocusColumeIdx = Hanoi_INVALID_NUMBER;
			}
			else
			{
				ColumeObjects[columeIdx]->SetSelect(true);
				ColumeObjects[FocusColumeIdx]->SetSelect(false);
				FocusColumeIdx = columeIdx;
			}
		}
	}
}

#undef LOCTEXT_NAMESPACE