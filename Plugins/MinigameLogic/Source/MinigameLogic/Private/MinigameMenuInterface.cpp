#include "MinigameMenuInterface.h"

AMinigameMenuEntry::AMinigameMenuEntry(const FObjectInitializer& initializer)
	: Super(initializer)
{}

AMinigameMenu::AMinigameMenu(const FObjectInitializer& initializer)
	: Super(initializer)
{}

AMinigameMenuEntry::~AMinigameMenuEntry()
{
	delete GameOption;
	GameOption = nullptr;
	delete GameScore;
	GameScore = nullptr;
}

void AMinigameMenuEntry::SetOwnerMenu(AMinigameMenu* MenuPtr)
{
	OwnerMenu = MenuPtr;

	if(GameScore)
		GameScore->SetOwnerMenu(MenuPtr);
}

void AMinigameMenuEntry::BeginPlay()
{
	Super::BeginPlay();

	if (GameScore)
		GameScore->SetOwnerMenu(OwnerMenu);

	OnStart();
}

void AMinigameMenuEntry::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	if (OwnerMenu != nullptr)
	{
		OwnerMenu->ShowMenu(true);
	}

	OnEnd();
}