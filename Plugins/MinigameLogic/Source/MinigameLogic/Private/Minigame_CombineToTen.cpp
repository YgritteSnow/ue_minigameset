// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "Minigame_CombineToTen.h"
#include "Internationalization/Internationalization.h"

static const FName CombineToTen_Option_Dimension("Dimision");
static const FName CombineToTen_Option_Goal("Goal");
static const FName CombineToTen_Option_Speed("Speed");
static const FName CombineToTen_Score_NumberSum("ScoreNumberSum");
static const FName CombineToTen_Score_BrickCount("ScoreBrickCount");
static const int CombineToTen_INIT_NUM = 0;
static const int CombineToTen_INVALID_NUM = -1;

#define LOCTEXT_NAMESPACE "Minigame_CombineToTen"

DEFINE_LOG_CATEGORY_STATIC(LogCombineToTen, Warning, All);

AMinigame_CombineToTen_Brick::AMinigame_CombineToTen_Brick(const FObjectInitializer& initializer)
	:Super(initializer)
{}

void FMinigame_CombineToTen_Options::InitDefault()
{
	InitParam_int(CombineToTen_Option_Dimension, LOCTEXT("CombineToTen", "Dimension"), 4);
	InitParam_int(CombineToTen_Option_Goal, LOCTEXT("CombineToTen", "Goal"), 10);
	InitParam_float(CombineToTen_Option_Speed, LOCTEXT("CombineToTen", "Speed"), 0.2);
}

void FMinigame_CombineToTen_Score::InitDefault()
{
	InitParam_int(CombineToTen_Score_NumberSum, LOCTEXT("CombineToTen", "NumberSum"), 0);
	InitParam_int(CombineToTen_Score_BrickCount, LOCTEXT("CombineToTen", "BlockCount"), 0);
}

AMinigame_CombineToTen::AMinigame_CombineToTen(const FObjectInitializer& initializer)
	: Super(initializer)
{
	GameOption = new FMinigame_CombineToTen_Options();
	GameScore = new FMinigame_CombineToTen_Score();
	GameOption->InitDefault();
	GameScore->InitDefault();
}

void AMinigame_CombineToTen::OnStart()
{
	Dimension = GameOption->GetParam_int(CombineToTen_Option_Dimension);
	DimensionSquare = Dimension * Dimension;
	Goal = GameOption->GetParam_int(CombineToTen_Option_Goal);
	Speed = GameOption->GetParam_float(CombineToTen_Option_Speed);

	ClearGameState();

	BrickObjects.Reset();
	BrickObjects.Reserve(DimensionSquare);
	for (int i = 0; i < DimensionSquare; ++i)
	{
		AMinigame_CombineToTen_Brick* brick = Cast<AMinigame_CombineToTen_Brick>(GetWorld()->SpawnActor(GameResource.BrickResourceTempl, &FVector::ZeroVector, &FRotator::ZeroRotator));
		check(brick != nullptr);

		brick->OnMousePress = FOnMousePress::CreateLambda([this, i]() {
			OnClickBrick(i);
		});
		brick->PosOffset = (Dimension - 1) * 0.5 * (brick->BrickSize + brick->BrickPadding);// - brick->BrickSize * 0.5;
		brick->MoveToPos(i / Dimension, i % Dimension, 0);
		BrickObjects.Add(brick);
	}


	SyncBrickStateImmediate();
}

void AMinigame_CombineToTen::OnRestart()
{
	ClearGameState();
	SyncBrickStateImmediate();
}

void AMinigame_CombineToTen::OnEnd()
{
	ClearGameState();
	for (int i = 0; i < DimensionSquare; ++i)
	{
		BrickObjects[i]->Destroy();
	}
	BrickObjects.Reset();
}

void AMinigame_CombineToTen::ClearGameState()
{
	FocusNumber = false;
	GameScore->InitDefault();
	BrickCountScore = 0;
	BrickNumberScore = 0;
	BrickStates.Reset();
	BrickStates.Reserve(DimensionSquare);
	SelectedBrickMasks.Reset();
	SelectedBrickMasks.Reserve(DimensionSquare);
	for (int i = 0; i < DimensionSquare; ++i)
	{
		BrickStates.Add(GetRandomNumber());
		SelectedBrickMasks.Add(false);
	}
}

void AMinigame_CombineToTen::SyncBrickStateImmediate()
{
	auto It_obj = BrickObjects.CreateConstIterator();
	auto It_state = BrickStates.CreateConstIterator();
	for (; It_state && It_obj; ++It_state, ++It_obj)
	{
		(*It_obj)->ShowNumber(*It_state);
	}
}

void AMinigame_CombineToTen::OnClickBrick(int brickIdx)
{
	bool isGameEnd = false;

	int brick_number = BrickStates[brickIdx];
	UE_LOG(LogCombineToTen, Warning, TEXT("OnClick idx=%d, number=%d, focus=%d"), brickIdx, brick_number, FocusNumber);
	if (FocusNumber == brick_number)
	{
		for (int i = 0; i < DimensionSquare; ++i)
		{
			if (SelectedBrickMasks[i])
			{
				BrickNumberScore += brick_number;
				++BrickCountScore;
				
				BrickStates[i] = (i == brickIdx ? brick_number + 1 : CombineToTen_INVALID_NUM);
				BrickObjects[i]->ShowNumber(BrickStates[i]);

				isGameEnd = isGameEnd || BrickStates[i] >= Goal;

				BrickObjects[i]->ShowSelected(false);
				SelectedBrickMasks[i] = false;
			}
		}

		CalcTargetBrick(MoveDir::Down);
		FocusNumber = CombineToTen_INVALID_NUM;
	}
	else
	{
		TArray<int> tocheckMask;
		TArray<bool> checkedMask;
		checkedMask.Reserve(DimensionSquare);
		for (int i = 0; i < DimensionSquare; ++i)
		{
			checkedMask.Add(false);
		}

		if (FocusNumber != CombineToTen_INVALID_NUM)
		{
			for (int i = 0; i < DimensionSquare; ++i)
			{
				if (SelectedBrickMasks[i])
				{
					BrickObjects[i]->ShowSelected(false);
					SelectedBrickMasks[i] = false;
				}
			}
		}

		int selected_count = 0;
		FocusNumber = brick_number;
		tocheckMask.Add(brickIdx);
		while (tocheckMask.Num() > 0)
		{
			int checkIdx = tocheckMask[0];
			tocheckMask.RemoveAt(0);
			checkedMask[checkIdx] = true;
			if (BrickStates[checkIdx] == brick_number)
			{
				++selected_count;
				SelectedBrickMasks[checkIdx] = true;
			}
			else
			{
				continue;
			}

			int x = checkIdx / Dimension;
			int y = checkIdx % Dimension;
			if (x > 0)
			{
				int tocheck = (x-1) * Dimension + y;
				if (!checkedMask[tocheck])
				{
					tocheckMask.Add(tocheck);
				}
			}
			if (x < Dimension -1)
			{
				int tocheck = (x + 1) * Dimension + y;
				if (!checkedMask[tocheck])
				{
					tocheckMask.Add(tocheck);
				}
			}
			if(y > 0)
			{
				int tocheck = (x) * Dimension + y-1;
				if (!checkedMask[tocheck])
				{
					tocheckMask.Add(tocheck);
				}
			}
			if (y < Dimension -1)
			{
				int tocheck = (x)* Dimension + y + 1;
				if (!checkedMask[tocheck])
				{
					tocheckMask.Add(tocheck);
				}
			}
		}

		if (selected_count == 1)
		{
			for (int i = 0; i < DimensionSquare; ++i)
			{
				SelectedBrickMasks[i] = false;
			}
		}
		else
		{
			for (int i = 0; i < DimensionSquare; ++i)
			{
				BrickObjects[i]->ShowSelected(SelectedBrickMasks[i]);
			}
		}
	}

	GameScore->SetParam_int(CombineToTen_Score_NumberSum, BrickNumberScore);
	GameScore->SetParam_int(CombineToTen_Score_BrickCount, BrickCountScore);

	if (isGameEnd)
	{
		OwnerMenu->GameEnd(1);
	}
}

void AMinigame_CombineToTen::MoveToPos(int brick_idx, int move_pos_x, int move_pos_y, float time)
{
	UE_LOG(LogCombineToTen, Warning, TEXT("MoveToPosA: idx=(%d,%d), to=(%d,%d), time=%.2f"), brick_idx / Dimension, brick_idx%Dimension, move_pos_x, move_pos_y, time);
	BrickObjects[brick_idx]->MoveToPos(move_pos_x, move_pos_y, time);
}

void AMinigame_CombineToTen::MoveToPos(int brick_idx, int move_pos, float time)
{
	UE_LOG(LogCombineToTen, Warning, TEXT("MoveToPosB: idx=(%d,%d), to=(%d,%d), time=%.2f"), brick_idx/Dimension,brick_idx%Dimension, move_pos / Dimension, move_pos%Dimension, time);
	BrickObjects[brick_idx]->MoveToPos(move_pos / Dimension, move_pos % Dimension, time);
}

void AMinigame_CombineToTen::CalcTargetBrick(MoveDir dir)
{
	FVector2D bgn_left;
	FVector2D lr_step;
	FVector2D be_step;
	switch (dir)
	{
	case MoveDir::Down:
		bgn_left = FVector2D(0, Dimension-1);
		lr_step = FVector2D(1, 0);
		be_step = FVector2D(0, -1);
		break;
	case MoveDir::Up:
		bgn_left = FVector2D(0, 0);
		lr_step = FVector2D(1, 0);
		be_step = FVector2D(0, 1);
		break;
	case MoveDir::Left:
		bgn_left = FVector2D(0, 0);
		lr_step = FVector2D(0, 1);
		be_step = FVector2D(1, 0);
		break;
	case MoveDir::Right:
		bgn_left = FVector2D(Dimension - 1, 0);
		lr_step = FVector2D(0, 1);
		be_step = FVector2D(-1, 0);
		break;
	default:
		return;
	};

	TArray<int> TargetBrickStates;
	TargetBrickStates.Reserve(DimensionSquare);
	for (int i = 0; i < DimensionSquare; ++i)
	{
		TargetBrickStates.Add(CombineToTen_INIT_NUM);
	}

	for (int lr_i = 0; lr_i < Dimension; ++lr_i)
	{
		for (int be_j = 0; be_j < Dimension; ++be_j)
		{
			FVector2D idx_pos = bgn_left + lr_i * lr_step + be_j * be_step;
			int real_idx = idx_pos.X * Dimension + idx_pos.Y;

			int next_search_posj = 0;
			if (BrickStates[real_idx] != CombineToTen_INVALID_NUM)
			{
				TargetBrickStates[real_idx] = BrickStates[real_idx];
				BrickStates[real_idx] = CombineToTen_INVALID_NUM;
				next_search_posj = be_j;
			}
			else
			{
				for (; next_search_posj < Dimension; ++next_search_posj)
				{
					FVector2D search_idx_pos = bgn_left + lr_i * lr_step + next_search_posj * be_step;
					int search_real_idx = search_idx_pos.X * Dimension + search_idx_pos.Y;

					if (BrickStates[search_real_idx] != CombineToTen_INVALID_NUM)
					{
						TargetBrickStates[real_idx] = BrickStates[search_real_idx];
						BrickStates[search_real_idx] = CombineToTen_INVALID_NUM;

						MoveToPos(real_idx, search_idx_pos.X, search_idx_pos.Y, 0);
						MoveToPos(real_idx, real_idx, Speed);
						break;
					}
				}
				if (next_search_posj >= Dimension)
				{
					FVector2D search_idx_pos = bgn_left + lr_i * lr_step + next_search_posj * be_step;

					TargetBrickStates[real_idx] = GetRandomNumber();
					MoveToPos(real_idx, search_idx_pos.X, search_idx_pos.Y, 0);
					MoveToPos(real_idx, real_idx, Speed);

					++next_search_posj;
				}
			}
		}
	}

	BrickStates = TargetBrickStates;
	for (int i = 0; i < DimensionSquare; ++i)
	{
		BrickObjects[i]->ShowNumber(BrickStates[i]);
	}
}

#undef LOCTEXT_NAMESPACE